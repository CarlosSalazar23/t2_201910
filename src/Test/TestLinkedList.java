package Test;

import junit.framework.TestCase;
import model.data_structures.LinkedList;

public class TestLinkedList extends TestCase
{

	LinkedList <String> x;

	public void setupEscenario0( )
	{
		x=new LinkedList<String>();
		x.add("P1");
		x.add("P2");
		x.add("P3");
	}

	public void testAdd( )
	{
		setupEscenario0( );
		assertEquals("No se est� a�adiendo al final", "P2", x.getCurrentElement(1));
	}
	public void testAddAtK()
	{
		setupEscenario0();
		x.addAtK("P4",1);
		assertEquals("No se est� a�adiendo en la posici�n adecuada", "P4", x.getCurrentElement(1));
	}

	public void testDelete()
	{
		setupEscenario0();
		x.delete();
		assertEquals("No se est� eliminando el �ltimo", null, x.getCurrentElement(2));
	}

	public void testDeleteAtK()
	{
		setupEscenario0();
		x.deleteAtK(1);
		assertEquals("No se est� eliminando en la posici�n adecuada", "P3", x.getCurrentElement(1));
	}

	public void testGetSize()
	{
		setupEscenario0();
		assertEquals("No es el tama�o real", "3", x.getSize()+"");
	}

	public void testGetCurrentElement()
	{
		setupEscenario0();
		assertEquals("No se est� retornando el elemento solicitado", "P2", x.getCurrentElement(1));
	}
}
