package model.logic;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;
import com.opencsv.CSVReader;

public class MovingViolationsManager implements IMovingViolationsManager {

	LinkedList<VOMovingViolations> movingViolations;
	VOMovingViolations model;

	public void loadMovingViolations(String movingViolationsFile)
	{
		movingViolations=new LinkedList<VOMovingViolations>();
		try 
		{
			CSVReader reader = new CSVReader(new FileReader(movingViolationsFile));
			String[] nextLine=reader.readNext();
			while ((nextLine = reader.readNext()) != null) 
			{				
				model=new VOMovingViolations(Integer.parseInt(nextLine[0]), nextLine[2], nextLine[13], Integer.parseInt(nextLine[9]),nextLine[12],nextLine[15],nextLine[14]);
				movingViolations.addAtEnd(model);
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public LinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		// TODO Auto-generated method stub
		LinkedList<VOMovingViolations> violationC = new LinkedList<VOMovingViolations>();

		for(int i=0;i<movingViolations.getSize();i++)
		{
			if(movingViolations.getCurrentElement(i).getViolationCode().compareToIgnoreCase(violationCode)==0)
			{
				violationC.add(movingViolations.getCurrentElement(i));
			}
		}
		return violationC;
	}

	public LinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) 
	{
		LinkedList<VOMovingViolations> violationsByAc = new LinkedList<VOMovingViolations>();

		for(int i=0;i<movingViolations.getSize();i++)
		{
			if(movingViolations.getCurrentElement(i).getAccidentIndicator().compareToIgnoreCase(accidentIndicator)==0)
			{
				violationsByAc.add(movingViolations.getCurrentElement(i));
			}
		}
		return violationsByAc;
	}	


}
