package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T>
{
	T getCurrentElement(int position);
	Integer getSize();
	T next();
	T previous();
	public void addAtEnd(T element);
	public void add (T element);
	public void addAtK(T element,int position);
	public void delete();
	public void deleteAtK(int position);
}
