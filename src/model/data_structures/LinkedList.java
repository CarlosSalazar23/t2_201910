package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements ILinkedList<T> 
{

	int longitud;
	ListNode<T> first;
	ListNode<T> prox;
	ListNode<T> actual;
	ListNode<T> last;
	public LinkedList()
	{
		first=null;
		prox=null;
		actual=null;
		last=null;
		longitud=0;
	}

	public Iterator iterator() 
	{
		return new ListIterator<T> ( first ); 
	}

	public T getCurrentElement(int position)
	{
		T element=null;
		boolean termino=false;
		actual=first;
		for(int i=0;i<longitud && !termino;i++)
		{
			if(i==position)
			{
				element= actual.getElement();
				termino=true;
			}
			actual=actual.getNext();
		}
		return element;
	}

	@Override
	public Integer getSize()
	{
		return longitud;
	}

	@Override
	public T next() 
	{
		return actual.getNext().getElement();
	}

	@Override
	public T  previous() {
		return actual.getPrev().getElement();
	}

	@Override
	public void addAtEnd(T element) 
	{
		ListNode<T> x=new ListNode<T> (element);
		if(first==null)
		{
			first=x;
			last=first;
		}
		else 
		{
			last.changeNext(x);
			x.changePrev(last);
			last=last.getNext();
		}
		longitud++;
	}

	@Override
	public void add(T element) 
	{
		addAtEnd(element);
	}

	public void addAtK(T element, int position) 
	{
		actual=first;
		int x=0;
		ListNode<T> y=new ListNode<T>(element);
		while(x<position)
		{
			actual=actual.getNext();
			x++;
		}
		y.changeNext(actual);
		actual.getPrev().changeNext(y);
		y.changePrev(actual.getPrev());
		actual.changePrev(y);
		longitud++;
	}

	@Override
	public void delete() 
	{
		while(actual!=null)
		{
			if(actual.getNext()==null)
			{
				actual.getPrev().changeNext(null);
			}
			else
			{
				actual=actual.getNext();
				delete();	
			}
		}
		longitud--;
	}

	@Override
	public void deleteAtK(int position) 
	{
		int x=0;
		actual=first;
		while(x<position)
		{
			actual=actual.getNext();
			x++;
		}
		actual.getPrev().changeNext(actual.getNext());
		actual.getNext().changePrev(actual.getPrev());
		longitud--;
	}

	class ListIterator<T> implements Iterator<T> 
	{
		ListNode<T> prox;
		ListNode<T> prev_prox;
		ListNode<T> prev_prev;

		ListIterator(ListNode<T> first)
		{
			prox=first;
		}

		public boolean hasNext()
		{
			return prox!=null;
		}

		public T next() 
		{
			if ( prox == null )
			{ throw new NoSuchElementException("No hay proximo"); }
			T element = prox.getElement();
			prox= prox.getNext();
			return element;
		}
	}
	public class ListNode<T>
	{
		private T element;
		private ListNode<T> next;
		private ListNode<T> prev;

		public ListNode(T pElement)
		{
			element=pElement;
			next=null;
			prev=null;
		}

		public void changeNext(ListNode<T> pNext)
		{
			next= pNext;
		}

		public void changePrev(ListNode<T> pPrev)
		{
			prev= pPrev;
		}

		public T getElement()
		{
			return element;
		}

		public ListNode<T> getNext()
		{
			return next;
		}

		public ListNode<T> getPrev()
		{
			return prev;
		}
	}

}
